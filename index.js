const fs = require('fs');
const storageVendor = require('./storage');
const multer = require('multer');
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, '/tmp')
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + '.' + file.originalname.split('.').pop())
    }
})

const upload = multer({ storage: storage })

const { server, app } = require('./generateServer')();

app.get('/health', (req, res) => {
    // Validate can interact with storage bucket
    storageVendor.getBucketLocation()
    .then(data => {
        return res.status(200).send('OK');
    })
    .catch(err => {
        console.log(err);
        return res.status(503).send('Invalid bucket configuration');
    })
})

app.post('/', upload.any(), (req, res) => {
    const { files } = req;
    const { directoryName, fileName } = req.body;

    if (!files || files.length === 0 ) {
        return res.status(400).send('Empty file');
    }
    if (files.length > 1) {
        return res.status(400).send('Only one file allowed per upload');
    }
    if (!directoryName || !fileName) {
        return res.status(400).send('directoryName|fileName required fields');
    }

    const file = files[0];
    
    storageVendor.saveFile(directoryName, fileName, fs.createReadStream(file.path))
    .then(({ url, Key, data }) => {
        return res.json({ url, Key, data });
    })
    .then(() => {
        fs.unlink(file.path, (err) => {
            if (err) {
                console.log(err);
            }
        })
    })
    .catch(err => {
        console.log('error uploading file', err);
        return res.status(400).send(err.message);
    })
})

app.delete('/*', (req, res) => {
    console.log('deleting file', req.url.substr(1))
    storageVendor.deleteFile(req.url.substr(1))
    .then((data) => {
        return res.json(data);
    })
    .catch(err => {
        console.log('error deleting file', err);
        return res.status(400).send(err.message);
    })
})



const PORT = process.env.PORT || 4000;
server.listen(PORT)
console.log(`Magic happens on port ${PORT}`)       // shoutout to the user
console.log(`==== Running in ${process.env.NODE_ENV} mode ===`)
exports = module.exports = app             // expose app
